import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';

import {UserModalComponent} from '../user-modal/user-modal.component';
import {UserModalDeleteComponent } from '../user-modal-delete/user-modal-delete.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  constructor(private api: ApiService, private modal: NgbModal) { }
  _modalRef;
  stateSpinner = false;
  users$;
  type = [{id: 1, description: 'Usuario'},
          {id: 2, description: 'Empleado'}];
  typeRed;
  groups$;
  groupRed;

  ngOnInit() {
    this.users$ = this.api.getUserAll();
    this.groups$ = this.api.getGroupAll();

    this.typeRed = this.type.reduce((acc, type) => {
      acc[type.id] = type;
      return acc;
    },{});

    this.groups$.subscribe(group => {
      this.groupRed = group.reduce((acc, gr) => {
        acc[gr.ID] = gr;

        return acc;
      },{});

    })
  }

  editUserModal(id, typeId) {

    this._modalRef = this.modal.open(UserModalComponent);
    // console.log('-----', id)
    this._modalRef.componentInstance.userId = id;
    this._modalRef.componentInstance.type = this.typeRed[typeId];
    this._modalRef.result.then(
      result => {
        // do nothing
      },
      updateUser => {
        if (updateUser) {
          this.stateSpinner = true;

          this.api.updateUser(id, updateUser).subscribe(x => x);
        }

        setTimeout( () => {
          this.stateSpinner = false;
          this.apiLoad();
        }, 1000);
      }
    );

  }

  deleteUserModal(id) {
    this._modalRef = this.modal.open(UserModalDeleteComponent);
    this._modalRef.componentInstance.userId = id;
    this._modalRef.result.then(
      result => {
        // do nothing
      },
      deleteUserId => {
        this.stateSpinner = true;
        this.api.deleteUser(deleteUserId).subscribe(x => x);
        setTimeout( () => {
          this.stateSpinner = false;
          this.apiLoad();
        }, 1000);
      });

  }

  apiLoad() {
    this.users$ = this.api.getUserAll();
  }

}
