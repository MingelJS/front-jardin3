import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { ApiService } from '../../../../services/api.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private api: ApiService,
    private notifierService: NotifierService,
    private router:Router) { }

  _userForm;
  optionValue = 0;
  groups;
  stateSpinner = false;
  notification = false;
  private readonly notifier: NotifierService;
  ngOnInit() {
   // this.notifier = notifierService;

    this._userForm = this.fb.group({
      USER_NAME: ['', Validators.required],
      EMAIL: ['', Validators.required],
      ADDRESS: ['', Validators.required],
      PASSWORD: ['', Validators.required],
      TYPE_ID: ['', Validators.required],
      GROUP_ID: ['', Validators.required],
    });

    this.groups = this.api.getGroupAll();


  }

  changeOption(e) {
    this.optionValue = parseInt(e.target.value);
  };


  addUser() {
    this.stateSpinner = true;
    let group_idOption;
    if( this.optionValue === 2) {

      group_idOption = parseInt(this._userForm.get('GROUP_ID').value);
    } else {
      group_idOption = 0;
    }
    let user_save = {
      user_name: this._userForm.get('USER_NAME').value,
      email: this._userForm.get('ADDRESS').value,
      address: this._userForm.get('EMAIL').value,
      password: this._userForm.get('PASSWORD').value,
      type_id: parseInt(this._userForm.get('TYPE_ID').value),
      group_id: group_idOption,
    };




    this.api.createUser(user_save).subscribe(x => console.log(x));
    setTimeout( () => {
      this.stateSpinner = false;
      this.router.navigate(['/userPage']);
    }, 1000);

  }

}
