import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {
  _userForm;
  userId;
  user$;
  userRed;
  groups$;
  user;
  type;
  constructor(private api: ApiService, private fb: FormBuilder, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.user$ = this.api.getUser(this.userId);
    this._userForm = this.fb.group({
      USER_NAME: ['', Validators.required],
      EMAIL: ['', Validators.required],
      ADDRESS: ['', Validators.required],
      PASSWORD: ['', Validators.required],
      TYPE_ID: ['', Validators.required],
      GROUP_ID: ['', Validators.required],
    });
    this.user$.subscribe(x => {
      this.user = x;
      if(this.user.length > 0) {

        this._userForm.patchValue({
          USER_NAME: this.user[0].USER_NAME,
          EMAIL: this.user[0].EMAIL,
          ADDRESS: this.user[0].ADDRESS,
          PASSWORD: this.user[0].PASSWORD,
          TYPE_ID: this.user[0].TYPE_ID,
          GROUP_ID: this.user[0].GROUP_ID,
        });

      }
    })


    this.groups$ = this.api.getGroupAll();

  }

  formatAndDismiss() {
    let user_save;

      user_save = {
        user_name: this._userForm.get('USER_NAME').value,
        email: this._userForm.get('EMAIL').value,
        address: this._userForm.get('ADDRESS').value,
        password: this._userForm.get('PASSWORD').value,
        type_id: parseInt(this._userForm.get('TYPE_ID').value),
        group_id:  parseInt(this._userForm.get('GROUP_ID').value),

    }
    this.activeModal.dismiss(user_save);
  }
}
