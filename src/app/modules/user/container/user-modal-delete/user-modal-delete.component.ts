import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-user-modal-delete',
  templateUrl: './user-modal-delete.component.html',
  styleUrls: ['./user-modal-delete.component.css']
})
export class UserModalDeleteComponent implements OnInit {

  constructor(private api: ApiService, public activeModal: NgbActiveModal) { }
  userId;
  ngOnInit() {
  }

  formatAndDismiss() {
    this.activeModal.dismiss(this.userId);
  }

}
