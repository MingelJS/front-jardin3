import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../services/api.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.css']
})
export class ServiceFormComponent implements OnInit {

  constructor(private fb: FormBuilder,private router:Router, private api: ApiService) { }

  _groupForm;

  ngOnInit() {

    this._groupForm = this.fb.group({
      GROUP_NAME: ['', Validators.required],
      GRUOP_STATUS_ID: ['', Validators.required],
      GROUP_DESCRIPTION: ['', Validators.required],
    });
  }


  addGroup() {

    let group_save = {
    group_name: this._groupForm.get('GROUP_NAME').value,
    gruop_status_id: this._groupForm.get('GRUOP_STATUS_ID').value,
    group_description: this._groupForm.get('GROUP_DESCRIPTION').value,
  };

    this.api.createGroupAdd(group_save).subscribe(x => console.log(x));
    this.router.navigate(['/home']);
  }

}
