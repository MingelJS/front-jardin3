import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Group } from '../../../../models/group';
import * as GroupAction from '../../../core/actions/group.actions';
import * as fromGroup from '../../../core/reducers/group.reducer';
import { select, Store } from '@ngrx/store';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-main-services',
  templateUrl: './main-services.component.html',
  styleUrls: ['./main-services.component.css']
})
export class MainServicesComponent implements OnInit {

  constructor( private store: Store<any>,private api: ApiService, private http: HttpClient ) { }

  groups;
  status = [{id: 1, description: 'Disponible'},
            {id: 0, description: 'No disponible'}];

  statusRed;
  ngOnInit() {

    this.statusRed = this.status.reduce((acc,sta) => {
      acc[sta.id] = sta.description;

      return acc;
    },{});

    // this.store.select(fromGroup.getAll).subscribe( g => console.log(g));
    this.groups = this.api.getGroupAll();
  }


}
