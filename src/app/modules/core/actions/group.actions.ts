import { Action } from '@ngrx/store';
import { Group } from '../../../models/group';

export enum GroupActionTypes {
  Load = '[API] Load groups',
  LoadSuccess = '[API] Load groups success',
  LoadFail = '[API] Load groups fail'
}

export class Load implements Action {
  readonly type = GroupActionTypes.Load;
  constructor() {}
}

export class LoadSuccess implements Action {
  readonly type = GroupActionTypes.LoadSuccess;
  constructor(public alarms: Group[]) {}
}

export class LoadFail implements Action {
  readonly type = GroupActionTypes.LoadFail;
  constructor(public error: any) {}
}





export type GroupActionsUnion =
  | Load
  | LoadFail
  | LoadSuccess;
