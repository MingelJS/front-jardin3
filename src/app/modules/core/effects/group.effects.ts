import { Injectable } from '@angular/core';
import { ApiService } from '../../../services/api.service.js';
import {
  LoadFail,
  LoadSuccess,
  GroupActionTypes

} from '../../core/actions/group.actions';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';


@Injectable()
export class GroupEffects {
  constructor(private actions$: Actions, private Api: ApiService) {}

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(GroupActionTypes.Load),
    switchMap(() =>
      this.Api.getGroupAll().pipe(
        map(groups => new LoadSuccess(groups)),
        catchError(error => of(new LoadFail(error)))
      )
    )
  );


}
