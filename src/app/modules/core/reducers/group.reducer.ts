import { Group } from '../../../models/group';
import {
  GroupActionsUnion,
  GroupActionTypes
} from '../actions/group.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface State extends EntityState<Group> {
  loading: boolean;
  loaded: boolean;
  data: Group[];
}

export const adapter: EntityAdapter<Group> = createEntityAdapter<Group>({
  selectId: (group: Group) => group.id
});

export const INITIAL_STATE: State = adapter.getInitialState({
  loading: false,
  loaded: false,
  data: []
});

export function reducer(
  state: State = INITIAL_STATE,
  action: GroupActionsUnion
) {
  switch (action.type) {
    case GroupActionTypes.Load:
      return { ...state, loading: true };

    case GroupActionTypes.LoadFail:
      return { ...state, loading: false };

    case GroupActionTypes.LoadSuccess:
      return { ...state, loading: false, loaded: true };
    default:
      return state;
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

export const selectLoaded = (state: State) => state.loaded;
export const selectLoading = (state: State) => state.loading;

export const getState = createFeatureSelector<State>('group');
export const getGroups = (state: State) => state.data;
export const getAll = createSelector(getState, selectAll);
export const getEntities = createSelector(getState, selectEntities);
