import * as fromGroup from './group.reducer';

import { ActionReducerMap } from '@ngrx/store';

export interface State {
  group: fromGroup.State;
}

export const reducers: ActionReducerMap<State> = {
  group: fromGroup.reducer,
}
