import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { LayoutFooterComponent } from '../layout/components/layout-footer/layout-footer.component';
import { LayoutHeaderComponent } from '../layout/components/layout-header/layout-header.component';
import { MainLayoutComponent } from '../main-layout/../layout/main-layout/main-layout.component';
import { effects } from './effects';
import { reducers } from './reducers';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { CookieService } from 'ngx-cookie-service';


export function logger(reducer: any): any {
  // default, no options
}

// export const metaReducers = [logger];
export const metaReducers = [];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    // StoreDevtoolsModule.instrument({
      // maxAge: 2
    // }),
  ],
  declarations: [
    MainLayoutComponent,
    LayoutHeaderComponent,
    LayoutFooterComponent
  ],
  entryComponents: [],
  providers: [
    CookieService
  ],

  exports: [MainLayoutComponent]
})
export class CoreModule {}
