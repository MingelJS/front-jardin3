import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Group } from '../models/group';
import { User } from '../models/users';
import { environment } from '../../environments/environment';
import { Store } from '@ngrx/store';

const API =  'http://127.0.0.1:3000/api/';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private store: Store<any>) {}

  getGroupAll() {
    return this.http.get<Group[]>(`${API}group`);
  }

  getUserAll() {
    return this.http.get<User[]>(`${API}user`);
  }

  getUser(id) {
    return this.http.get<User>(`${API}user/${id}`);
  }

  updateUser(id, data) {

    return this.http.put<User>(`${API}user/${id}`, data);
  }

  deleteUser(id) {
    console.log('id', id);
    return this.http.delete<User>(`${API}user/${id}`);
  }

  createGroupAdd(data) {

    return this.http.post<Group>(`${API}group`, data);
  }

  createUser(data) {
    return this.http.post<User>(`${API}user`, data);
  }
}
