import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { ApiService } from './api.service.js';
import { environment } from '../../environments/environment';
import { reducers } from '../modules/core/reducers';
import { StoreModule } from '@ngrx/store';

describe('ApiService', () => {
  let injector: TestBed;
  let api: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, StoreModule.forRoot(reducers)],
      providers: [ApiService]
    });
    injector = getTestBed();
    api = injector.get(ApiService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('nothing', done => {
    done();
  });

});
