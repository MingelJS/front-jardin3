import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MainServicesComponent } from './modules/services-layout/containers/main-services/main-services.component';
import { LayoutBodyComponent } from './modules/layout/components/layout-body/layout-body.component';
import { ServiceFormComponent } from './modules/services-layout/service-form/service-form.component';
import { RegisterComponent} from './modules/user/container/register/register.component';
import { UserPageComponent } from './modules/user/container/user-page/user-page.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  {
    path: 'home',
    component: LayoutBodyComponent
  },
{ path: 'services', component: MainServicesComponent },
{ path: 'addServices', component: ServiceFormComponent },
{ path: 'userAdd', component: RegisterComponent },
{ path: 'userPage', component: UserPageComponent }
];
// const routes: Routes = [];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
