import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotifierModule } from 'angular-notifier';

import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './modules/layout/main-layout/main-layout.component';
import { LayoutHeaderComponent } from './modules/layout/components/layout-header/layout-header.component';
import { LayoutBodyComponent } from './modules/layout/components/layout-body/layout-body.component';
import { LayoutFooterComponent } from './modules/layout/components/layout-footer/layout-footer.component';
import { MainServicesComponent } from './modules/services-layout/containers/main-services/main-services.component';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './modules/core/core.module';
import { ServiceFormComponent } from './modules/services-layout/service-form/service-form.component';
import { RegisterComponent } from './modules/user/container/register/register.component';
import { UserPageComponent } from './modules/user/container/user-page/user-page.component';
import { UserModalComponent } from './modules/user/container/user-modal/user-modal.component';
import { SpinnerComponent } from './modules/shared/spinner/spinner.component';
import { UserModalDeleteComponent } from './modules/user/container/user-modal-delete/user-modal-delete.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutBodyComponent,
    MainServicesComponent,
    ServiceFormComponent,
    RegisterComponent,
    UserPageComponent,
    UserModalComponent,
    SpinnerComponent,
    UserModalDeleteComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    CoreModule,
    ReactiveFormsModule,
    NotifierModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [UserModalComponent, UserModalDeleteComponent]
})
export class AppModule { }
