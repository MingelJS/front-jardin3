export const environment = {
  production: false,
  API: 'http://127.0.0.1:3000/api/',
  SOCKET_URL: 'http://127.0.0.1:3000',
  SOCKET_PATH: null
};
